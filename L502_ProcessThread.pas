unit L502_ProcessThread;

interface
uses Classes, Math, SyncObjs,StdCtrls,SysUtils, l502api;
  const RECV_BUF_SIZE = 8*1024*1024;
  const RECV_TOUT     = 250;

type TL502_ProcessThread = class(TThread)
  public
    LChEdits : array of TEdit;
    dinResEdit : TEdit;
    hnd: t_l502_hnd; //��������� ������

    err : LongInt; //��� ������ ��� ���������� ������ �����
    stop : Boolean; //������ �� ������� (��������������� �� ��������� ������)

    constructor Create(SuspendCreate : Boolean);
    destructor Free();

  private
     { Private declarations }
    adcData : array [0..RECV_BUF_SIZE-1] of double;
    dinData : array [0..RECV_BUF_SIZE-1] of LongWord;
    adcSize, dinSize, firstLch : LongWord;


    procedure updateData;
  protected
    procedure Execute; override;
  end;
implementation


  constructor TL502_ProcessThread.Create(SuspendCreate : Boolean);
  begin
     Inherited Create(SuspendCreate);
     stop:=False;
     hnd:=Nil;
     err:=L502_ERR_OK;
  end;

  destructor TL502_ProcessThread.Free();
  begin
      Inherited Free();
  end;

  { ���������� ����������� ����� ������������ ���������� ���������.
   ����� ������ ����������� ������ ����� Syncronize, ������� �����
   ��� ������� � ��������� VCL �� �� ��������� ������ }
  procedure TL502_ProcessThread.updateData;
  var
    lch_cnt, i : LongWord;
    err : LongInt;
  begin
    err := L502_GetLChannelCount(hnd, lch_cnt);
    if err=L502_ERR_OK then
    begin
      // ������������� � ����������� �������� ������� ������� �� �������
      for  i:=0 to Min(lch_cnt, adcSize) do
        LChEdits[(firstLch + i) mod lch_cnt].Text := FloatToStrF(adcData[i], ffFixed, 4, 8);

      { ���� ���� ������ ��������� ������, �� ������������� ���������
            � ������������ � ������ ��������� }
      if dinSize <> 0 then
        dinResEdit.Text := IntToHex(dinData[0] and $3FFFF, 5)
      else
        dinResEdit.Text := '';

    end;
  end;

  procedure TL502_ProcessThread.Execute;
  var
    stoperr, rcv_size : LongInt;
    rcv_buf : array of LongWord;
  begin
    setlength(rcv_buf, RECV_BUF_SIZE);
    // ��������� ���������� ������
    err:= L502_StreamsStart(hnd);
    if err = L502_ERR_OK then
    begin
      { ��������� ����� ���� �� ���������� ������ ���
        �� ����� ������� �� ������� �� ��������� ���������� }
      while not stop and (err = L502_ERR_OK) do
      begin
        //��������� ������ ����������� �����
        rcv_size := L502_Recv(hnd, rcv_buf, RECV_BUF_SIZE, RECV_TOUT);
        // �������� ������ ���� �������� ������...
        if rcv_size < 0 then
            err := rcv_size
        else if rcv_size>0 then
        begin
          //���� ������ ���� - ������ ������� ����� ������
          dinSize:=RECV_BUF_SIZE;
          adcSize:=RECV_BUF_SIZE;
          { �������� ����� ���. �������, ��������������� �������
            ������� ���, ��� ��� �� ����� ����� ����������
            ��������� ���������� ������ }
          err:=L502_GetNextExpectedLchNum(hnd, firstLch);
          if err = L502_ERR_OK then
          begin
            { ��������� ������ �� ���������� ���� � ������� ��� �
              ��������� ��� � ������ }
            err := L502_ProcessData(hnd, rcv_buf, rcv_size, L502_PROC_FLAGS_VOLT,
                                    adcData, adcSize, dinData, dinSize);
            if err = L502_ERR_OK then
            begin
              // ��������� �������� ��������� ����������
              Synchronize(updateData);
            end;
          end;
        end
      end;

      { �� ������ �� ����� ������������� �����.
        ����� �� �������� ��� ������ (���� ����� �� ������)
        ��������� �������� ��������� � ��������� ���������� }
      stoperr:= L502_StreamsStop(hnd);
      if err = L502_ERR_OK then
        err:= stoperr;
    end;
  end;
end.
