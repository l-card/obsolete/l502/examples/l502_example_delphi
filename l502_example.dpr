program l502_example;

uses
  Forms,
  MainUnit in 'MainUnit.pas' {MainForm},
  L502_ProcessThread in 'L502_ProcessThread.pas',
  l502api in 'l502api.pas',
  lpcieapi in 'lpcieapi.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.
