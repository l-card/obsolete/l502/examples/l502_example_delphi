unit MainUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,
  l502api, L502_ProcessThread;

type
  TMainForm = class(TForm)
    btnRefreshDeviceList: TButton;
    cbbSerialList: TComboBox;
    btnOpen: TButton;
    grpDevInfo: TGroupBox;
    chkDacPresent: TCheckBox;
    chkGalPresent: TCheckBox;
    chkDspPresent: TCheckBox;
    edtFpgaVer: TEdit;
    edtPldaVer: TEdit;
    lbl1: TLabel;
    lbl2: TLabel;
    btnAsyncDigIn: TButton;
    edtAsyncDigIn: TEdit;
    btnAsyncDigOut: TButton;
    edtAsyncDigOut: TEdit;
    grp1: TGroupBox;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl5: TLabel;
    btnSetAdcFreq: TButton;
    edtAdcFreq: TEdit;
    edtAdcFreqLch: TEdit;
    edtDinFreq: TEdit;
    grp2: TGroupBox;
    lbl6: TLabel;
    lbl7: TLabel;
    lbl8: TLabel;
    lbl9: TLabel;
    lbl10: TLabel;
    cbbLCh1_Channel: TComboBox;
    cbbLCh1_Range: TComboBox;
    cbbLCh1_Mode: TComboBox;
    edtLCh1_Result: TEdit;
    cbbLCh2_Channel: TComboBox;
    cbbLCh2_Range: TComboBox;
    cbbLCh2_Mode: TComboBox;
    edtLCh2_Result: TEdit;
    cbbLCh3_Channel: TComboBox;
    cbbLCh3_Range: TComboBox;
    cbbLCh3_Mode: TComboBox;
    edtLCh3_Result: TEdit;
    chkSyncDin: TCheckBox;
    edtDin_Result: TEdit;
    cbbLChCnt: TComboBox;
    btnAsyncAdcFrame: TButton;
    btnStop: TButton;
    btnStart: TButton;
    btnAsyncDac1: TButton;
    edtAsyncDac1: TEdit;
    edtAsyncDac2: TEdit;
    btnAsyncDac2: TButton;
    GroupBox4: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    cbbSyncMode: TComboBox;
    cbbSyncStartMode: TComboBox;
    procedure btnRefreshDeviceListClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnOpenClick(Sender: TObject);
    procedure btnAsyncDigInClick(Sender: TObject);
    procedure btnAsyncDigOutClick(Sender: TObject);
    procedure btnSetAdcFreqClick(Sender: TObject);
    procedure btnAsyncAdcFrameClick(Sender: TObject);
    procedure btnStartClick(Sender: TObject);
    procedure btnAsyncDac1Click(Sender: TObject);
    procedure btnAsyncDac2Click(Sender: TObject);
    procedure btnStopClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure chkSyncDinClick(Sender: TObject);
  private
    hnd : t_l502_hnd; //��������� ������
    threadRunning : Boolean; //�������, ������� �� ����� ����� ������
    thread: TL502_ProcessThread; //����� ��� ����������� �����

    procedure updateControls();
    procedure refreshDeviceList();
    procedure closeDevice();
    procedure OnThreadTerminate(par : TObject);
    function setAdcFreq() : LongInt;
    function setupParams() : LongInt;
    function setSyncDinStream() : LongInt;

    { Private declarations }
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}

procedure TMainForm.updateControls();
begin
  //������ �������� ������ �������� ���� �� ������� ����������
  btnRefreshDeviceList.Enabled := hnd=nil;
  cbbSerialList.Enabled:= hnd=nil;

  if hnd = nil then
    btnOpen.Caption := '���������� ����� � �������'
  else
    btnOpen.Caption := '��������� ����� � �������';
  btnOpen.Enabled := (hnd<>nil) or (cbbSerialList.ItemIndex >= 0);

  chkSyncDin.Enabled := (hnd<>nil) and not threadRunning;

  btnStart.Enabled := (hnd<>nil) and not threadRunning;
  btnStop.Enabled  := (hnd<>nil) and threadRunning;

  btnSetAdcFreq.Enabled := (hnd<>nil) and not threadRunning;

  btnAsyncDigOut.Enabled := (hnd<>nil);
  btnAsyncDigIn.Enabled := (hnd<>nil);
  btnAsyncDac1.Enabled := (hnd<>nil) and chkDacPresent.Checked;
  btnAsyncDac2.Enabled := (hnd<>nil) and chkDacPresent.Checked;
  btnAsyncAdcFrame.Enabled := (hnd<>nil) and not threadRunning;
end;



{ ������� ��������� ������ �������� ������� ������������ ��������� }
procedure TMainForm.refreshDeviceList();
var
  //��������� �� ������ �������� �������, ������� ������� �����������
  serial_list : array of string;
  res,i : LongInt;
  devcnt : LongWord;
begin
  cbbSerialList.Items.Clear;
  { �������� ���������� ������� L502 � �������.
    serial_list �� ��������� ������� ������������ ����� ������ ������
    � ������� ���������� }
  res := L502_GetSerialList(serial_list, 0, devcnt);
  if (res>=0) and (devcnt > 0) then
  begin
    //������������� ������ �������, ����� ��� ������� ��� ������� ���-�� �������
    SetLength(serial_list, devcnt);
    //�������� �������� ������ �������
    res := L502_GetSerialList(serial_list, 0);
    if res>=0 then
    begin
      for i:=0 to res-1 do
        cbbSerialList.Items.Add(string(serial_list[i]));
      cbbSerialList.ItemIndex := 0;
    end;
  end;

  if res<0 then
    MessageDlg(L502_GetErrorString(res), mtError, [mbOK], 0);


  updateControls;
end;



// ��������� ���������� ����� ������ � ������������ � ���������� GUI
function TMainForm.setupParams() : Integer;
var
  err : Integer;
  lch_cnt : LongWord;
const
  { ������� ������������ �������� � ComboBox � ����� ������ ��������� }
  mode_tbl: array[0..2] of LongWord = (L502_LCH_MODE_COMM, L502_LCH_MODE_DIFF, L502_LCH_MODE_ZERO);
  { ������� ������������ �������� � ComboBox � ����� ���������� ��� }
  range_tbl: array[0..5] of LongWord = (L502_ADC_RANGE_10, L502_ADC_RANGE_5, L502_ADC_RANGE_2,
                                    L502_ADC_RANGE_1, L502_ADC_RANGE_05, L502_ADC_RANGE_02);
  { ������� ������������ �������� � ComboBox � ����� ��������� ������������� }
  f_sync_tbl: array[0..5] of LongWord = (L502_SYNC_INTERNAL, L502_SYNC_EXTERNAL_MASTER,
                                    L502_SYNC_DI_SYN1_RISE, L502_SYNC_DI_SYN2_RISE,
                                    L502_SYNC_DI_SYN1_FALL, L502_SYNC_DI_SYN2_FALL);
begin
  lch_cnt := cbbLChCnt.ItemIndex + 1;
  // ������������� ���-�� ���������� �������
  err := L502_SetLChannelCount(hnd, lch_cnt);

  // ������������ ������� ���������� �������
  if err = L502_ERR_OK then
  begin
    err := L502_SetLChannel(hnd,0, cbbLCh1_Channel.ItemIndex,
            mode_tbl[cbbLCh1_Mode.ItemIndex], range_tbl[cbbLCh1_Range.ItemIndex],0);
  end;
  if (err = L502_ERR_OK) and (lch_cnt>=2) then
  begin
    err := L502_SetLChannel(hnd,1, cbbLCh2_Channel.ItemIndex,
            mode_tbl[cbbLCh2_Mode.ItemIndex], range_tbl[cbbLCh2_Range.ItemIndex],0);
  end;
  if (err = L502_ERR_OK) and (lch_cnt>=3) then
  begin
    err := L502_SetLChannel(hnd,2, cbbLCh3_Channel.ItemIndex,
            mode_tbl[cbbLCh3_Mode.ItemIndex], range_tbl[cbbLCh3_Range.ItemIndex],0);
  end;

  // ����������� �������� ������� ������������� � ������� �����
	if err = L502_ERR_OK then
		err := L502_SetSyncMode(hnd, f_sync_tbl[cbbSyncMode.ItemIndex]);
  if err = L502_ERR_OK then
    err := L502_SetSyncStartMode(hnd, f_sync_tbl[cbbSyncStartMode.ItemIndex]);

  // ����������� ������� ����� � ���
  if err = L502_ERR_OK then
    err := setAdcFreq();

  // ���������� ��������� � ������
  if err = L502_ERR_OK then
    err := L502_Configure(hnd, 0);

  setupParams:= err;
end;



procedure TMainForm.closeDevice();
begin
  if hnd<>nil then
  begin
    //���� ������� ����� ����� ������ - �� ������������� ���
    if threadRunning then
    begin
      thread.stop:=True;
      thread.WaitFor;
    end;
    // �������� ����� � �������
    L502_Close(hnd);
    // ������������ ���������
    L502_Free(hnd);
    hnd := nil;
  end;
end;

//��������� ������� ��� � ������������ �� ����������
//��������� ����������, � ���������� �� � ������������
//� ������� ��������������
function TMainForm.setAdcFreq() : Integer;
var f_acq, f_lch, f_din: Double;
    err : Integer;
begin
    f_acq := StrToFloat(edtAdcFreq.Text);
    f_lch := StrToFloat(edtAdcFreqLch.Text);
    f_din := StrToFloat(edtDinFreq.Text);
    // ������������� ��������� ������� �����.
    err := L502_SetAdcFreq(hnd, f_acq, f_lch);
    if err = L502_ERR_OK then
    begin
      // ��������� �������� ����������, ��� �����������
      // ������� �������������� �������
      edtAdcFreq.Text := FloatToStr(f_acq);
      edtAdcFreqLch.Text := FloatToStr(f_lch);
      // ������������� ������� ����������� �����
      err := L502_SetDinFreq(hnd, f_din);
      if err = L502_ERR_OK then
        edtDinFreq.Text := FloatToStr(f_din);
    end;
    setAdcFreq:= err;
end;

function TMainForm.setSyncDinStream : LongInt;
var err : LongInt;
begin
    { ��������� ��� ��������� ����� ����������� �����
       � ����������� �� ��������� ������������� }
    if chkSyncDin.Checked then
        err := L502_StreamsEnable(hnd, L502_STREAM_DIN)
    else
        err := L502_StreamsDisable(hnd, L502_STREAM_DIN);

   setSyncDinStream:= err;
end;

procedure TMainForm.btnRefreshDeviceListClick(Sender: TObject);
begin
  refreshDeviceList;
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  hnd := nil;
  refreshDeviceList;
end;

procedure TMainForm.btnOpenClick(Sender: TObject);
var
  serial: string;
  info : t_l502_info;
  err : Integer;
begin
  if hnd=nil then
  begin
    //������� ���������
    hnd := L502_Create();
    if hnd=nil then
      MessageDlg('������ �������� ��������� ������',mtError, [mbOK], 0)
    else
    begin
      serial:= cbbSerialList.Items[cbbSerialList.ItemIndex];
      // ��������� ������ � ��������� �������� �������
      err := L502_Open(hnd, serial);
      if err <> L502_ERR_OK then
      begin
        // �� ������ ������� => ����������� ���������
        MessageDlg('������ �������� ������: ' + L502_GetErrorString(err),mtError, [mbOK], 0);
        L502_Free(hnd);
        hnd := nil;
      end
      else
      begin
        //�������� ���������� � ����������
        err := L502_GetDevInfo(hnd,info);
        if err <> L502_ERR_OK then
        begin
          MessageDlg('������ ��������� ���������� � ����������: ' + L502_GetErrorString(err),
                     mtError, [mbOK], 0);
          //��� ������ ��������� ����� � �����������
          L502_Close(hnd);
          L502_Free(hnd);
          hnd := nil;
        end
        else
        begin
          //������� ������������ ���������� �� ����������
          chkDacPresent.Checked := Boolean(info.devflags and L502_DEVFLAGS_DAC_PRESENT);
          chkGalPresent.Checked := Boolean(info.devflags and L502_DEVFLAGS_GAL_PRESENT);
          chkDspPresent.Checked := Boolean(info.devflags and L502_DEVFLAGS_BF_PRESENT);

          edtPldaVer.Text := IntToStr(info.plda_ver);
          edtFpgaVer.Text := IntToStr((info.fpga_ver shl 8) and $FF) + '.' +
                              IntToStr(info.fpga_ver and $FF);
        end;
      end;
    end;
  end
  else
  begin
     //���� ���������� ���� ������� - ���������
     closeDevice;
  end;

  updateControls;
end;

procedure TMainForm.btnAsyncDigInClick(Sender: TObject);
var din: Cardinal;
var err: Integer;
begin
  if hnd<>nil then
  begin
    err := L502_AsyncInDig(hnd, din);
    if err<>L502_ERR_OK then
        MessageDlg('������ ������������ ����� � �������� �����: ' + L502_GetErrorString(err),
                     mtError, [mbOK], 0)
    else
        edtAsyncDigIn.Text := IntToHex(din and $3FFFF, 5);
  end;                                                     
end;

procedure TMainForm.btnAsyncDigOutClick(Sender: TObject);
var dout: Cardinal;
var err: Integer;
begin
  if hnd<>nil then
  begin
    dout:= StrToInt(edtAsyncDigOut.Text);
    err := L502_AsyncOutDig(hnd, dout, 0);
    if err <> L502_ERR_OK then
       MessageDlg('������ ������������ ������ �������� �����: ' + L502_GetErrorString(err),
                     mtError, [mbOK], 0)
  end;
end;

procedure TMainForm.btnSetAdcFreqClick(Sender: TObject);
var err : Integer;
begin
  { ������������� ���-�� ���������� �������, ����� ���������
    ��������� ������� �� ����� }
  err := L502_SetLChannelCount(hnd, cbbLChCnt.ItemIndex+1);
  if err = L502_ERR_OK then
    err := setAdcFreq();
  if err <> L502_ERR_OK then
    MessageDlg('������ ��������� �������: ' + L502_GetErrorString(err),
                     mtError, [mbOK], 0)
end;

procedure TMainForm.btnAsyncAdcFrameClick(Sender: TObject);
var err : Integer;
    adc_data : array of double;
    lch_cnt : LongWord;

begin
    if hnd <> nil then
    begin
      // ������������� ��������� ������
      err := setupParams();
      if err = L502_ERR_OK then
        err := L502_GetLChannelCount(hnd, lch_cnt);

      if err = L502_ERR_OK then
      begin
        // ������������� ������ ������� ������ ���������� ���������� �������
        SetLength(adc_data, lch_cnt);
        err := L502_AsyncGetAdcFrame(hnd, L502_PROC_FLAGS_VOLT, 1000, adc_data);

        if err = L502_ERR_OK then
        begin
          //������� ���������
          edtLCh1_Result.Text := FloatToStrF(adc_data[0], ffFixed, 4, 8);
          if lch_cnt >= 2 then
            edtLCh2_Result.Text := FloatToStrF(adc_data[1], ffFixed, 4, 8)
          else
            edtLCh2_Result.Text := '';

          if lch_cnt >= 3 then
            edtLCh3_Result.Text := FloatToStrF(adc_data[2], ffFixed, 4, 8)
          else
            edtLCh3_Result.Text := '';

        end
        else
        begin
           MessageDlg('������ ������ ����� ���: ' + L502_GetErrorString(err),
                     mtError, [mbOK], 0);
        end
      end
      else
      begin
        MessageDlg('������ ��������� ���������� ���: ' + L502_GetErrorString(err),
                     mtError, [mbOK], 0);
      end;
    end;
end;

procedure TMainForm.btnStartClick(Sender: TObject);
var
  err : LongInt;
begin
  //����������� ��� ��������� � ������������ � ���������� ����������
  err := setupParams();
  //��������� ���������� ���� ��� 
  if err = L502_ERR_OK then
     err := L502_StreamsEnable(hnd, L502_STREAM_ADC);

  // ��������� ���������� ���� � �������� ����� � ����������� �� �������������
  if err = L502_ERR_OK then
    err := setSyncDinStream();

  if err <> L502_ERR_OK then
  begin
    MessageDlg('������ ��������� ���������� ������: ' + L502_GetErrorString(err),
                     mtError, [mbOK], 0);
  end
  else
  begin
    if thread <> nil then
    begin
      FreeAndNil(thread);
    end;

    thread := TL502_ProcessThread.Create(True);
    thread.hnd := hnd;
    SetLength(thread.LChEdits, 3);
    thread.LChEdits[0] := edtLCh1_Result;
    thread.LChEdits[1] := edtLCh2_Result;
    thread.LChEdits[2] := edtLCh3_Result;
    thread.dinResEdit  := edtDin_Result;

    edtLCh1_Result.Text:= '';
    edtLCh2_Result.Text:= '';
    edtLCh3_Result.Text:= '';
    edtDin_Result.Text:= '';
    { ������������� ������� �� ������� ���������� ������ (� ���������
    ����� ���������, ���� ����� ���������� ��� ��-�� ������ ��� �����
    ������) }
    thread.OnTerminate := OnThreadTerminate;
    thread.Resume; //��� Delphi 2010 � ���� ������������� ������������ Start
    threadRunning := True;

    updateControls;
  end;

end;

//�������, ���������� �� ���������� ������ ����� ������
//��������� ����� ������, ������������� thread_run
procedure TMainForm.OnThreadTerminate(par : TObject);
begin
    if thread.err <> L502_ERR_OK then
    begin
        MessageDlg('���� ������ �������� � �������: ' + L502_GetErrorString(thread.err),
                  mtError, [mbOK], 0);
    end;

    threadRunning := false; 
    updateControls();
end;

procedure TMainForm.btnAsyncDac1Click(Sender: TObject);
var
  err : LongInt;
  val : Double;
begin
  if hnd<>nil then
  begin
    val := StrToFloat(edtAsyncDac1.Text);;
    err := L502_AsyncOutDac(hnd, L502_DAC_CH1, val, L502_DAC_FLAGS_CALIBR or
                                                        L502_DAC_FLAGS_VOLT);
    if err <> L502_ERR_OK then
    begin
       MessageDlg('������ ������ �� ���: ' + L502_GetErrorString(thread.err),
                  mtError, [mbOK], 0);
    end;
  end;
end;

procedure TMainForm.btnAsyncDac2Click(Sender: TObject);
var
  err : LongInt;
  val : Double;
begin
  if hnd<>nil then
  begin
    val := StrToFloat(edtAsyncDac2.Text);;
    err := L502_AsyncOutDac(hnd, L502_DAC_CH2, val, L502_DAC_FLAGS_CALIBR or
                                                        L502_DAC_FLAGS_VOLT);
    if err <> L502_ERR_OK then
    begin
       MessageDlg('������ ������ �� ���: ' + L502_GetErrorString(thread.err),
                  mtError, [mbOK], 0);
    end;
  end;
end;

procedure TMainForm.btnStopClick(Sender: TObject);
begin
    // ������������� ������ �� ���������� ������
    if threadRunning then
        thread.stop:=True;
    btnStop.Enabled:= False;
end;

procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  closeDevice;
  if thread <> nil then
    FreeAndNil(thread);
end;

procedure TMainForm.chkSyncDinClick(Sender: TObject);
var err : LongInt;
begin
  if hnd<>nil then
  begin
    err := setSyncDinStream();
    if err <> L502_ERR_OK then
    begin
      MessageDlg('������ ����������/������� ����������� �����: ' + L502_GetErrorString(thread.err),
                  mtError, [mbOK], 0);      
    end;
  end;
end;

end.
